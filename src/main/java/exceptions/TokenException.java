package exceptions;

public class TokenException extends Exception {

    public TokenException(String session) {
        super(session);
    }
    
}
