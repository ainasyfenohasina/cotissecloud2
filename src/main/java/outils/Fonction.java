
package outils;

import classes.PlaceClient;
import classes.Reservation;
import classes.Reservation;
import classes.OptionsReservation;
import connection.ServiceDAO;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class Fonction {
    
   public List<Integer> avoirNumero(String numero){
       List<Integer> maliste = new ArrayList<Integer>();
       String[] splitOne = numero.split(";");
       for(int a=0; a<splitOne.length; a++){
           String[] temp = splitOne[a].split("-");
           if(temp.length == 1){
               maliste.add(Integer.parseInt(temp[0]));
           }
           else{
               int debut = Integer.parseInt(temp[0]);
               int fin = Integer.parseInt(temp[1]);
               for(int b=debut; b<=fin; b++){
                   maliste.add(b);
               }
           }
       }
       return maliste;
   }
   
   public void fairereservation(Connection connection, String idVoyage, String idClient, String idVoiture, String numeroConcat, double pu, String idOpCocat) throws Exception{
       ServiceDAO ser = new ServiceDAO();
       
       //initiation des donnees
       List<Integer> places = this.avoirNumero(numeroConcat);
       int placesSize = places.size();
       String idreservation = "R"+ser.getSequence(connection, "MaSeq");
       
       if(placesSize!=0){
           //etape 1 inserer reservation
            Reservation NewReservation = new Reservation();
            NewReservation.setIdReservation(idreservation);
            NewReservation.setIdVoyage(idVoyage);
            NewReservation.setIdClient(idClient);
            NewReservation.setDateReservation(ManipulationDate.getNow());
            NewReservation.setNombrePlace((double)placesSize);
            NewReservation.setPu(pu);
            NewReservation.setEtat(1);

            NewReservation.insert(connection);

            //etape 2 inserer les places
            for(int a=0; a<placesSize; a++){
                PlaceClient temp = new PlaceClient();
                temp.setIdPlaceClient("PCR"+ser.getSequence(connection, "MaSeq"));
                temp.setIdVoyage(idVoyage);
                temp.setIdClient(idClient);
                temp.setIdVoiture(idVoiture);
                temp.setNumeroPlace(places.get(a));
                temp.setEtat(1);

                temp.insert(connection);
            }

            //etape 3 inserer les options
            if(idOpCocat!=""){
                String[] idOption = idOpCocat.split(";");
                for(int a=0; a<idOption.length; a++){
                    OptionsReservation tempOP = new OptionsReservation();
                    tempOP.setIdOptionsReservation("OPR"+ser.getSequence(connection, "MaSeq"));
                    tempOP.setIdOptions(idOption[a]);
                    tempOP.setIdReservation(idreservation);
                    tempOP.insert(connection);
                }
            }
       }
   } 

}
