package classes;

public class InsereReservationObjet {
    String idVoyage;
    String idClient;
    String idVoiture;
    String numeroConcat;
    double pu;
    String idOpCocat;
    
    //constructor
    public InsereReservationObjet(){}
    
    //getter and setter

    public String getIdVoyage() {
        return idVoyage;
    }

    public void setIdVoyage(String idVoyage) {
        this.idVoyage = idVoyage;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getIdVoiture() {
        return idVoiture;
    }

    public void setIdVoiture(String idVoiture) {
        this.idVoiture = idVoiture;
    }

    public String getNumeroConcat() {
        return numeroConcat;
    }

    public void setNumeroConcat(String numeroConcat) {
        this.numeroConcat = numeroConcat;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    public String getIdOpCocat() {
        return idOpCocat;
    }

    public void setIdOpCocat(String idOpCocat) {
        this.idOpCocat = idOpCocat;
    }
    
    
}
