package classes;

import java.sql.Timestamp;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.ServiceDAO;
import outils.EncryptService;
import outils.ManipulationDate;

public class Client {
	String idClient;
	String nom;
	String prenom;
	String numeroTel;
	String mdp;
	String sexe;
	int etat;
        
	//constructor
	public Client() {}

        
        //method
        public void insert(Connection connection)throws Exception{
            String query = "insert into Client ( idClient, nom, prenom, numeroTel, mdp, sexe, etat) values ('"+this.idClient+"', '"+this.nom+"', '"+this.prenom+"', '"+this.numeroTel+"', '"+this.mdp+"', '"+this.sexe+"', "+this.etat+")";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.execute();
            }catch(Exception e){
                throw e;
            }finally{
                ps.close();
            }
        }
        
        public List<Client> select(Connection connection, String complement)throws Exception{
            List<Client> result = new ArrayList<Client>();
            String Query = "select * from Client "+complement;
            PreparedStatement preparedStat = connection.prepareStatement(Query);
            ResultSet resultSet = null;
            try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    Client pc = new Client();
                    pc.setIdClient(resultSet.getString("idIdClient"));
                    pc.setNom(resultSet.getString("nom"));
                    pc.setPrenom(resultSet.getString("prenom"));
                    pc.setNumeroTel(resultSet.getString("numeroTel"));
                    pc.setMdp(resultSet.getString("mdp"));
                    pc.setSexe(resultSet.getString("pu"));                    
                    pc.setEtat(resultSet.getInt("etat"));
                    result.add(pc);
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
            return result;
        }
        
        public void Update(Connection connection, String elementToSet)throws Exception{
            String query = "Update Client set "+elementToSet+" where idClient like '"+this.idClient+"'";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.executeQuery();
            }catch(Exception e){
            
            }finally{
                ps.close();
            }
        }
		
	//method token
	public Token generateToken(Connection connection, String idUser) throws SQLException, Exception{
		System.out.println("tafiditra generateToken");
        Timestamp now = ManipulationDate.getNow();
        Timestamp expiration = now;
        expiration.setHours(expiration.getHours()+1);
        Token token = new Token(EncryptService.md5Hashing(this.getNumeroTel()+EncryptService.md5Hashing(now.toString())), idUser, expiration);
		token.insert(connection);
        return token;
    }
	
	//login
    public Client connectionUser(Connection connection) throws SQLException, Exception{
        Client user = new Client();
        String sql = "select * from Client where numeroTel like ? and mdp like ?";
        PreparedStatement preparedStat = connection.prepareStatement(sql);
        
        try{
            preparedStat.setString(1, this.getNumeroTel());
            preparedStat.setString(2, this.getMdp());
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                user.setNumeroTel(resultSet.getString("numeroTel"));
                user.setIdClient(resultSet.getString("idClient"));
            }
            resultSet.close();
            if(user.getNumeroTel()==null){
                throw new Exception("Email ou mot de passe invalide");
            }
        }
        catch(Exception e){
            throw e;
        }finally{
            preparedStat.close();
        }
        return user;
    }

	//inscryption
	public Token inscription(Connection connection)throws Exception{

		ServiceDAO DAO = new ServiceDAO();
        Token token = new Token();
        PreparedStatement preparedStat = null;
        String idclient = "C"+DAO.getSequence(connection, "MaSeq");
        try{
            String sql = "insert into client values(?, ?, ?, ?, ?, ?, ?)";
            
            preparedStat = connection.prepareStatement(sql);
            
        	preparedStat.setString(1, idclient);
        	preparedStat.setString(2, nom);
        	preparedStat.setString(3, prenom);
        	preparedStat.setString(4, numeroTel);
        	preparedStat.setString(5, mdp);
        	preparedStat.setString(6, sexe);
        	preparedStat.setInt(7, etat);
            preparedStat.executeQuery();
            

        }
        catch(Exception e){
            throw e;
        }finally{
            preparedStat.close();
            token = this.generateToken(connection, idclient);
            System.out.println("token finaly"+token);
            return token;
        }

    }
		
	//getter and setter
	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNumeroTel() {
		return numeroTel;
	}

	public void setNumeroTel(String numeroTel) {
		this.numeroTel = numeroTel;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}
	
}
