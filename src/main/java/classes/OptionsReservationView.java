package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class OptionsReservationView {

    String idOptionsReservation;
    String idOptions;
    String idReservation;
    String libelle;
    double prix;
    int etat;

    //constructor
    public OptionsReservationView() {
    }

    //method
    public void insert(Connection connection) throws Exception {
        String query = "insert into OptionsReservation ( idOptionsReservation, idOptions, idReservation) values ('"+this.idOptionsReservation+"', '"+this.idOptions+"', '"+this.idReservation+"')";
        PreparedStatement ps = connection.prepareStatement(query);
        try {
            ps.execute();
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
        }
    }

    public List<OptionsReservationView> select(Connection connection, String complement) throws Exception {
        List<OptionsReservationView> result = new ArrayList<OptionsReservationView>();
        String Query = "select * from OptionsReservationView " + complement;
        PreparedStatement preparedStat = connection.prepareStatement(Query);
        ResultSet resultSet = null;
        try {
            resultSet = preparedStat.executeQuery();
            while (resultSet.next()) {
                OptionsReservationView pc = new OptionsReservationView();
                pc.setIdOptionsReservation(resultSet.getString("idOptionsReservation"));
                pc.setIdOptions(resultSet.getString("idOptions"));
                pc.setIdReservation(resultSet.getString("idReservation"));
                pc.setLibelle(resultSet.getString("libelle"));
                pc.setPrix(resultSet.getDouble("prix"));
                pc.setEtat(resultSet.getInt("etat"));
                result.add(pc);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            preparedStat.close();
            resultSet.close();
        }
        return result;
    }

    public void Update(Connection connection, String elementToSet) throws Exception {
        String query = "Update OptionsReservation set " + elementToSet + " where idOptionsReservation like '" + this.idOptionsReservation + "'";
        PreparedStatement ps = connection.prepareStatement(query);
        try {
            ps.executeQuery();
        } catch (Exception e) {

        } finally {
            ps.close();
        }
    }

    //getter and setter
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getIdOptionsReservation() {
        return idOptionsReservation;
    }

    public void setIdOptionsReservation(String idOptionsReservation) {
        this.idOptionsReservation = idOptionsReservation;
    }

    public String getIdOptions() {
        return idOptions;
    }

    public void setIdOptions(String idOptions) {
        this.idOptions = idOptions;
    }

    public String getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(String idReservation) {
        this.idReservation = idReservation;
    }
}
