package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Reservation {
	String idReservation;
	String idVoyage;
	String idClient;
	Timestamp dateReservation;
	double nombrePlace;
	double pu;
	int etat;
	
        //method
        public void insert(Connection connection)throws Exception{
            String query = "insert into Reservation ( idReservation, IdVoyage, idClient, dateReservation, nombrePlace, pu, etat) values ('"+this.idReservation+"', '"+this.idVoyage+"', '"+this.idClient+"', '"+this.dateReservation+"', "+this.nombrePlace+", "+this.pu+", "+this.etat+")";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.execute();
            }catch(Exception e){
                throw e;
            }finally{
                ps.close();
            }
        }
        public List<Reservation> select(Connection connection, String complement)throws Exception{
            List<Reservation> result = new ArrayList<Reservation>();
            String Query = "select * from Reservation "+complement;
            PreparedStatement preparedStat = connection.prepareStatement(Query);
            ResultSet resultSet = null;
            try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    Reservation pc = new Reservation();
                    pc.setIdReservation(resultSet.getString("idReservation"));
                    pc.setIdVoyage(resultSet.getString("idVoyage"));
                    pc.setIdClient(resultSet.getString("idClient"));
                    pc.setDateReservation(resultSet.getTimestamp("dateReservation"));
                    pc.setNombrePlace(resultSet.getInt("nombrePlace"));
                    pc.setPu(resultSet.getDouble("pu"));                    
                    pc.setEtat(resultSet.getInt("etat"));
                    result.add(pc);
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
            return result;
        }
        public void Update(Connection connection, String elementToSet)throws Exception{
            String query = "Update Reservation set "+elementToSet+" where idReservation like '"+this.idReservation+"'";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.executeQuery();
            }catch(Exception e){
            
            }finally{
                ps.close();
            }
            
        }
        
	//constructo1r
	public Reservation() {}

	//getter and setter
	public String getIdReservation() {
		return idReservation;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public void setIdReservation(String idReservation) {
		this.idReservation = idReservation;
	}

	public String getIdVoyage() {
		return idVoyage;
	}

	public void setIdVoyage(String idVoyage) {
		this.idVoyage = idVoyage;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public Timestamp getDateReservation() {
		return dateReservation;
	}

	public void setDateReservation(Timestamp dateReservation) {
		this.dateReservation = dateReservation;
	}

	public double getNombrePlace() {
		return nombrePlace;
	}

	public void setNombrePlace(double nombrePlace) {
		this.nombrePlace = nombrePlace;
	}

	public double getPu() {
		return pu;
	}

	public void setPu(double pu) {
		this.pu = pu;
	}
	
}
