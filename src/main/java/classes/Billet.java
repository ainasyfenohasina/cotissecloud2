package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Billet {
	String idBillet;
	String idReservation;
	String nom;
	String prenom;
	String depart;
	String arriver;
	Timestamp dateDepart;
	double numeroVoiture;
	double nombrePlace;
	String numeroSiege;
	double prix;
        
        
        public void insert(Connection connection)throws Exception{
            String query = "insert into Billet ( idBillet, IdReservation, nom, prenom, depart, arriver, dateDepart,numeroVoiture,nombrePlace,numeroSiege,prix) values ('"+this.idBillet+"','"+this.idReservation+"','"+this.nom+"','"+this.prenom+"','"+this.depart+"','"+this.arriver+"','"+this.dateDepart+"',"+this.numeroVoiture+","+this.nombrePlace+",'"+this.numeroSiege+"',"+this.prix+")";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.execute();
            }catch(Exception e){
                throw e;
            }finally{
                ps.close();
            }
        }
        
        public List<Billet> select(Connection connection, String complement)throws Exception{
            List<Billet> result = new ArrayList<Billet>();
            String Query = "select * from Billet "+complement;
            PreparedStatement preparedStat = connection.prepareStatement(Query);
            ResultSet resultSet = null;
            try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    Billet pc = new Billet();
                    pc.setIdBillet(resultSet.getString("idBillet"));
                    pc.setIdReservation(resultSet.getString("idReservation"));
                    pc.setNom(resultSet.getString("nom"));
                    pc.setPrenom(resultSet.getString("prenom"));
                    pc.setArriver(resultSet.getString("arriver"));
                    pc.setDateDepart(resultSet.getTimestamp("dateDepart"));
                    pc.setNumeroVoiture(resultSet.getDouble("numeroVoiture"));
                    pc.setNombrePlace(resultSet.getDouble("nombrePlace"));                    
                    pc.setNumeroSiege(resultSet.getString("numeroSiege"));                    
                    pc.setPrix(resultSet.getDouble("prix"));
                    result.add(pc);
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
            return result;
        }
        
        public void Update(Connection connection, String elementToSet)throws Exception{
            String query = "Update Billet set "+elementToSet+" where idBillet like '"+this.idBillet+"'";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.executeQuery();
            }catch(Exception e){
            
            }finally{
                ps.close();
            }
        }
        
	//constructor 
	public Billet() {}

	//getter and setter
	public String getIdBillet() {
		return idBillet;
	}

	public void setIdBillet(String idBillet) {
		this.idBillet = idBillet;
	}

	public String getIdReservation() {
		return idReservation;
	}

	public void setIdReservation(String idReservation) {
		this.idReservation = idReservation;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getDepart() {
		return depart;
	}

	public void setDepart(String depart) {
		this.depart = depart;
	}

	public String getArriver() {
		return arriver;
	}

	public void setArriver(String arriver) {
		this.arriver = arriver;
	}

	public Timestamp getDateDepart() {
		return dateDepart;
	}

	public void setDateDepart(Timestamp dateDepart) {
		this.dateDepart = dateDepart;
	}

	public double getNumeroVoiture() {
		return numeroVoiture;
	}

	public void setNumeroVoiture(double numeroVoiture) {
		this.numeroVoiture = numeroVoiture;
	}

	public double getNombrePlace() {
		return nombrePlace;
	}

	public void setNombrePlace(double nombrePlace) {
		this.nombrePlace = nombrePlace;
	}

	public String getNumeroSiege() {
		return numeroSiege;
	}

	public void setNumeroSiege(String numeroSiege) {
		this.numeroSiege = numeroSiege;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}
	
	
}
