package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PlaceClient {
	String idPlaceClient;
	String idVoyage;
	String idClient;
	String idVoiture;
	int numeroPlace;
	int etat;
	
        //Methods
        public void insert(Connection connection)throws Exception{
            String query = "insert into PlaceClient( idPlaceClient, IdVoyage, idClient, idVoiture, numeroPlace, etat) values ('"+this.idPlaceClient+"', '"+this.idVoyage+"', '"+this.idClient+"', '"+this.idVoiture+"', '"+this.numeroPlace+"', "+this.etat+")";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.execute();
            }catch(Exception e){
                throw e;
            }finally{
                ps.close();
            }
        }
        public List<PlaceClient> select(Connection connection, String complement)throws Exception{
            List<PlaceClient> result = new ArrayList<PlaceClient>();
            String Query = "select * from PlaceClient "+complement;
            PreparedStatement preparedStat = connection.prepareStatement(Query);
            ResultSet resultSet = null;
            try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    PlaceClient pc = new PlaceClient();
                    pc.setIdPlaceClient(resultSet.getString("idPlaceClient"));
                    pc.setIdVoyage(resultSet.getString("idVoyage"));
                    pc.setIdClient(resultSet.getString("idClient"));
                    pc.setIdVoiture(resultSet.getString("idVoiture"));
                    pc.setNumeroPlace(resultSet.getInt("numeroPlace"));
                    pc.setEtat(resultSet.getInt("etat"));
                    result.add(pc);
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
            return result;
        }
        public void Update(Connection connection, String elementToSet)throws Exception{
            String query = "Update PlaceClient set "+elementToSet+" where idPlaceClient like '"+this.idPlaceClient+"'";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.executeQuery();
            }catch(Exception e){
            
            }finally{
                ps.close();
            }
            
        }
        
	//constructor
	public PlaceClient() {}

	//getter and setter
	public String getIdPlaceClient() {
		return idPlaceClient;
	}

	public void setIdPlaceClient(String idPlaceClient) {
		this.idPlaceClient = idPlaceClient;
	}

	public String getIdVoyage() {
		return idVoyage;
	}

	public void setIdVoyage(String idVoyage) {
		this.idVoyage = idVoyage;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public String getIdVoiture() {
		return idVoiture;
	}

	public void setIdVoiture(String idVoiture) {
		this.idVoiture = idVoiture;
	}

	public int getNumeroPlace() {
		return numeroPlace;
	}

	public void setNumeroPlace(int numeroPlace) {
		this.numeroPlace = numeroPlace;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}
	
}