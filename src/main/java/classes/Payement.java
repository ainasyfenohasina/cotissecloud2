package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Payement {
	String idPayement;
	String idReservation;
	String idClient;
	double montant;
	
	//constructor
	public Payement() {}

        //method
        public void insert(Connection connection)throws Exception{
            String query = "insert into Payement ( idPayement, IdReservation, idClient, montant) values ('"+this.idPayement+"', '"+this.idReservation+"', '"+this.idClient+"', "+this.montant+")";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.execute();
            }catch(Exception e){
                throw e;
            }finally{
                ps.close();
            }
        }
        
        public List<Payement> select(Connection connection, String complement)throws Exception{
            List<Payement> result = new ArrayList<Payement>();
            String Query = "select * from Payement "+complement;
            PreparedStatement preparedStat = connection.prepareStatement(Query);
            ResultSet resultSet = null;
            try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    Payement pc = new Payement();
                    pc.setIdPayement(resultSet.getString("idPayement"));
                    pc.setIdReservation(resultSet.getString("idReservation"));
                    pc.setIdClient(resultSet.getString("idClient"));
                    pc.setMontant(resultSet.getDouble("montant"));
                    result.add(pc);
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
            return result;
        }
        
        public void Update(Connection connection, String elementToSet)throws Exception{
            String query = "Update Payement set "+elementToSet+" where idPayement like '"+this.idPayement+"'";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.executeQuery();
            }catch(Exception e){
            
            }finally{
                ps.close();
            }
        }
        
	//getter and setter
	public String getIdPayement() {
		return idPayement;
	}

	public void setIdPayement(String idPayement) {
		this.idPayement = idPayement;
	}

	public String getIdReservation() {
		return idReservation;
	}

	public void setIdReservation(String idReservation) {
		this.idReservation = idReservation;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}
	
}
