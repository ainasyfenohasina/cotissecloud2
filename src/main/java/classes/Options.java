package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Options {
	String idOptions;
	String libelle;
	double prix;
	int etat;
	
	//constructor
	public Options() {}

        //method
        public void insert(Connection connection)throws Exception{
            String query = "insert into Options ( idOptions, libelle, prix, etat) values ('"+this.idOptions+"', '"+this.libelle+"', '"+this.prix+"', "+this.etat+")";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.execute();
            }catch(Exception e){
                throw e;
            }finally{
                ps.close();
            }
        }
        
        public List<Options> select(Connection connection, String complement)throws Exception{
            List<Options> result = new ArrayList<Options>();
            String Query = "select * from Options "+complement;
            PreparedStatement preparedStat = connection.prepareStatement(Query);
            ResultSet resultSet = null;
            try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    Options pc = new Options();
                    pc.setIdOptions(resultSet.getString("idOptions"));
                    pc.setLibelle(resultSet.getString("libelle"));
                    pc.setPrix(resultSet.getDouble("prix"));
                    pc.setEtat(resultSet.getInt("etat"));
                    result.add(pc);
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
            return result;
        }
        
        public void Update(Connection connection, String elementToSet)throws Exception{
            String query = "Update Options set "+elementToSet+" where idOptions like '"+this.idOptions+"'";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.executeQuery();
            }catch(Exception e){
            
            }finally{
                ps.close();
            }
        }
        
	//getter and setter
	public String getIdOptions() {
		return idOptions;
	}

	public void setIdOptions(String idOptions) {
		this.idOptions = idOptions;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}
	
}
