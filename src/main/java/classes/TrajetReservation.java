
package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TrajetReservation {
    String idReservation;
    String depart;
    String arriver;
    Timestamp dateDepart;
    Timestamp dateReservation; 
    String idClient;
    String nom; 
    String prenom;
    double pu;
    int nombrePlace; 
    int etat;
    
    //methode
    public List<TrajetReservation> select(Connection connection, String complement)throws Exception{
            List<TrajetReservation> result = new ArrayList<TrajetReservation>();
            String Query = "select * from TrajetReservation "+complement;
            PreparedStatement preparedStat = connection.prepareStatement(Query);
            ResultSet resultSet = null;
            try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    TrajetReservation pc = new TrajetReservation();
                    pc.setIdReservation(resultSet.getString("idReservation"));
                    pc.setDepart(resultSet.getString("depart"));
                    pc.setArriver(resultSet.getString("arriver"));                    
                    pc.setDateDepart(resultSet.getTimestamp("dateDepart"));
                    pc.setDateReservation(resultSet.getTimestamp("dateReservation"));
                    pc.setIdClient(resultSet.getString("idClient"));
                    pc.setNom(resultSet.getString("nom"));
                    pc.setPrenom(resultSet.getString("prenom"));
                    pc.setPu(resultSet.getDouble("pu"));
                    pc.setNombrePlace(resultSet.getInt("nombrePlace"));
                    pc.setEtat(resultSet.getInt("etat"));
                    result.add(pc);
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
            return result;
        }
    
    //constructor

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public double getPu() {
        return pu;
    }

    public void setPu(double pu) {
        this.pu = pu;
    }

    
    public Timestamp getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(Timestamp dateDepart) {
        this.dateDepart = dateDepart;
    }
        
    public String getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(String idreservation) {
        this.idReservation = idreservation;
    }
        
    public TrajetReservation(){}
    
    //getter and setter

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArriver() {
        return arriver;
    }

    public void setArriver(String arriver) {
        this.arriver = arriver;
    }

    public Timestamp getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(Timestamp dateReservation) {
        this.dateReservation = dateReservation;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getNombrePlace() {
        return nombrePlace;
    }

    public void setNombrePlace(int nombrePlace) {
        this.nombrePlace = nombrePlace;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    
}
