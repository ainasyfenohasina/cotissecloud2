package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Voiture {
	String idVoiture;
	String marque;
	String numeroVoiture;
	int taille;
	String positionArret;
	int disponiblite;
	
        //methode
        public void insert(Connection connection)throws Exception{
            String query = "insert into Voiture ( idVoiture, marque, numeroVoiture, taille, positionArret, disponiblite) values ('"+this.idVoiture+"', '"+this.marque+"', '"+this.numeroVoiture+"', "+this.taille+", '"+this.positionArret+"', "+this.disponiblite+")";
			System.out.println(query);
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.execute();
            }catch(Exception e){
                throw e;
            }finally{
                ps.close();
            }
        }
        public List<Voiture> select(Connection connection, String complement)throws Exception{
            List<Voiture> result = new ArrayList<Voiture>();
            String Query = "select * from Voiture "+complement;
            PreparedStatement preparedStat = connection.prepareStatement(Query);
            ResultSet resultSet = null;
            try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    Voiture pc = new Voiture();
                    pc.setIdVoiture(resultSet.getString("idVoiture"));
                    pc.setMarque(resultSet.getString("marque"));
                    pc.setNumeroVoiture(resultSet.getString("numeroVoiture"));
                    pc.setTaille(resultSet.getInt("taille"));
                    pc.setPositionArret(resultSet.getString("positionArret"));
                    pc.setDisponiblite(resultSet.getInt("disponiblite"));
                    result.add(pc);
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
            return result;
        }
        public void Update(Connection connection, String elementToSet)throws Exception{
            String query = "Update Voiture set "+elementToSet+" where idVoiture like '"+this.idVoiture+"'";
			System.out.println(query);
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.executeQuery();
            }catch(Exception e){
            
            }finally{
                ps.close();
            }
            
        }
        
	//constructor
	public Voiture() {}

	//getter et setter
	public String getIdVoiture() {
		return idVoiture;
	}

	public void setIdVoiture(String idVoiture) {
		this.idVoiture = idVoiture;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getNumeroVoiture() {
		return numeroVoiture;
	}

	public void setNumeroVoiture(String numeroVoiture) {
		this.numeroVoiture = numeroVoiture;
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	public String getPositionArret() {
		return positionArret;
	}

	public void setPositionArret(String positionArret) {
		this.positionArret = positionArret;
	}

	public int getDisponiblite() {
		return disponiblite;
	}

	public void setDisponiblite(int disponiblite) {
		this.disponiblite = disponiblite;
	}

}
