package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Voyage {

    String idVoyage;
    String idTrajet;
    String idVoiture;
    String dateDepart2;
    Timestamp dateDepart;
    double duree;
    double prix;
    int etat;

    //method
    public void insert(Connection connection) throws Exception {
        String query = "insert into Voyage( idVoyage, idTrajet, idVoiture, dateDepart, duree, prix, etat) values ('"+this.idVoyage+"', '"+this.idTrajet+"', '"+this.idVoiture+"', '"+this.dateDepart+"', "+this.duree+", "+this.prix+", 1)";
        System.out.print(query);
        PreparedStatement ps = connection.prepareStatement(query);
        try {
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            ps.close();
        }
    }

    public List<Voyage> select(Connection connection, String complement) throws Exception {
        List<Voyage> result = new ArrayList<Voyage>();
        String Query = "select * from Voyage " + complement;
        PreparedStatement preparedStat = connection.prepareStatement(Query);
        ResultSet resultSet = null;
        try {
            resultSet = preparedStat.executeQuery();
            while (resultSet.next()) {
                Voyage pc = new Voyage();
                pc.setIdVoyage(resultSet.getString("idVoyage"));
                pc.setIdTrajet(resultSet.getString("idTrajet"));
                pc.setIdVoiture(resultSet.getString("idVoiture"));
                pc.setDateDepart(resultSet.getTimestamp("dateDepart"));
                pc.setDuree(resultSet.getDouble("duree"));
                pc.setPrix(resultSet.getDouble("prix"));
                pc.setEtat(resultSet.getInt("etat"));
                result.add(pc);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            preparedStat.close();
            resultSet.close();
        }
        return result;
    }

    public void Update(Connection connection, String elementToSet) throws Exception {
        String query = "Update Voyage set " + elementToSet + " where idVoyage like '" + this.idVoyage + "'";
        PreparedStatement ps = connection.prepareStatement(query);
        try {
            ps.executeQuery();
        } catch (Exception e) {

        } finally {
            ps.close();
        }

    }

    //constructor
    public Voyage() {
    }

    //getter and setter

    public String getDateDepart2() {
        return dateDepart2;
    }

    public void setDateDepart2(String dateDepart2) {
        this.dateDepart2 = dateDepart2;
    }
    
    
    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getIdVoyage() {
        return idVoyage;
    }

    public double getDuree() {
        return duree;
    }

    public void setDuree(double duree) {
        this.duree = duree;
    }

    public void setIdVoyage(String idVoyage) {
        this.idVoyage = idVoyage;
    }

    public String getIdTrajet() {
        return idTrajet;
    }

    public void setIdTrajet(String idTrajet) {
        this.idTrajet = idTrajet;
    }

    public String getIdVoiture() {
        return idVoiture;
    }

    public void setIdVoiture(String idVoiture) {
        this.idVoiture = idVoiture;
    }

    public Timestamp getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(Timestamp dateDepart) {
        this.dateDepart = dateDepart;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

}
