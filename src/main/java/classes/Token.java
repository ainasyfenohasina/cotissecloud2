package classes;

import exceptions.TokenException;
import java.sql.Connection;
import java.sql.Timestamp;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import outils.ManipulationDate;


public class Token {
    String id;
    String token;
    String idClient;
    Timestamp expiration;

    public void verifValidity(Connection connection) throws SQLException, Exception{
        String query = "select * from Token where token like ?";
        PreparedStatement preparedStat = null;
        ResultSet resultSet = null;
        try{
            preparedStat = connection.prepareStatement(query);
            preparedStat.setString(1, this.getToken().trim());
            resultSet = preparedStat.executeQuery();
            Timestamp exp = null;
            while(resultSet.next()){
                exp = resultSet.getTimestamp("expiration");
            }
            
            Timestamp now = ManipulationDate.getNow();
            if(now.after(exp)){
                throw new TokenException("Session expiree");
            }
        }catch(SQLException ex){
            throw ex;
        }finally{
            if(resultSet!=null){
                resultSet.close();
            }
            if(preparedStat!=null){
                preparedStat.close();
            }
        }
    }
    
    public Token getToken(Connection connection) throws SQLException, Exception{
        String query = "select * from Token where token like ?";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        preparedStat.setString(1, this.getToken().trim());
        Token token = new Token();
        ResultSet resultSet =  preparedStat.executeQuery();
        try{
            token.setId(resultSet.getString("id"));
            token.setToken(resultSet.getString("token"));
            token.setExpiration(resultSet.getTimestamp("expiration"));
            
        }catch(Exception e){
            throw e;
        }finally{
            if(resultSet!=null){
                resultSet.close();
            }
            if(preparedStat!=null){
                preparedStat.close();
            }
        }
        return token;
    }
    
    public Token(String token){
        this.setToken(token);
    }
    
    public Token(String token, String idClient, Timestamp expiration){
        this.setToken(token);
        this.setIdClient(idClient);
        this.setExpiration(expiration);
    }
    public Token(){
        
    }
    
    public void insert(Connection connection) throws SQLException{
        String query = "insert into token(token, expiration, idClient) values( ?, ?, ?)";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setString(1, this.getToken());
            preparedStat.setTimestamp(2, this.getExpiration());
            preparedStat.setString(3, this.getIdClient());
            preparedStat.execute();
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
    }
    
    public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getExpiration() {
        return expiration;
    }

    public void setExpiration(Timestamp expiration) {
        this.expiration = expiration;
    }
    
    
}