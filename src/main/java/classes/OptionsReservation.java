package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class OptionsReservation {
	String idOptionsReservation;
	String idOptions;
	String idReservation;
	
	//constructor
	public OptionsReservation(){}
	
        //method
        public void insert(Connection connection)throws Exception{
            String query = "insert into OptionsReservation ( idOptionsReservation, idOptions, idReservation) values ('"+this.idOptionsReservation+"', '"+this.idOptions+"', '"+this.idReservation+"')";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.execute();
            }catch(Exception e){
                throw e;
            }finally{
                ps.close();
            }
        }
        
         public List<OptionsReservation> select(Connection connection, String complement)throws Exception{
            List<OptionsReservation> result = new ArrayList<OptionsReservation>();
            String Query = "select * from OptionsReservation "+complement;
            PreparedStatement preparedStat = connection.prepareStatement(Query);
            ResultSet resultSet = null;
            try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    OptionsReservation pc = new OptionsReservation();
                    pc.setIdOptionsReservation(resultSet.getString("idOptionsReservation"));
                    pc.setIdOptions(resultSet.getString("idOptions"));
                    pc.setIdReservation(resultSet.getString("idReservation"));
                    result.add(pc);
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
            return result;
        }
         
        public void Update(Connection connection, String elementToSet)throws Exception{
            String query = "Update OptionsReservation set "+elementToSet+" where idOptionsReservation like '"+this.idOptionsReservation+"'";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.executeQuery();
            }catch(Exception e){
            
            }finally{
                ps.close();
            }
        }
        
        
	//getter and setter
	public String getIdOptionsReservation() {
		return idOptionsReservation;
	}

	public void setIdOptionsReservation(String idOptionsReservation) {
		this.idOptionsReservation = idOptionsReservation;
	}

	public String getIdOptions() {
		return idOptions;
	}

	public void setIdOptions(String idOptions) {
		this.idOptions = idOptions;
	}

	public String getIdReservation() {
		return idReservation;
	}

	public void setIdReservation(String idReservation) {
		this.idReservation = idReservation;
	}
}
