package classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Trajet {
	String idTrajet;
	String depart;
	String arriver;
	double distance;
	
        //methode
        public void insert(Connection connection)throws Exception{
            String query = "insert into Trajet( idTrajet, depart, arriver, distance) values ('"+this.idTrajet+"', '"+this.depart+"', '"+this.arriver+"', "+this.distance+")";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.execute();
            }catch(Exception e){
                throw e;
            }finally{
                ps.close();
            }
        }
        public List<Trajet> select(Connection connection, String complement)throws Exception{
            List<Trajet> result = new ArrayList<Trajet>();
            String Query = "select * from Trajet "+complement;
            PreparedStatement preparedStat = connection.prepareStatement(Query);
            ResultSet resultSet = null;
            try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    Trajet pc = new Trajet();
                    pc.setIdTrajet(resultSet.getString("idTrajet"));
                    pc.setDepart(resultSet.getString("depart"));
                    pc.setArriver(resultSet.getString("arriver"));
                    pc.setDistance(resultSet.getDouble("distance"));
                    result.add(pc);
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
            return result;
        }
        public void Update(Connection connection, String elementToSet)throws Exception{
            String query = "Update Trajet set "+elementToSet+" where idTrajet like '"+this.idTrajet+"'";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.executeQuery();
            }catch(Exception e){
            
            }finally{
                ps.close();
            }
            
        }
        public void delete(Connection connection)throws Exception{
            String query = "delete from Trajet where idTrajet like '"+this.idTrajet+"'";
            PreparedStatement ps = connection.prepareStatement(query);
            try{
                ps.executeQuery();
            }catch(Exception e){
            
            }finally{
                ps.close();
            }
		}
        
	//constructor
	public Trajet() {}

	//getter and setter
	public String getIdTrajet() {
		return idTrajet;
	}

	public void setIdTrajet(String idTrajet) {
		this.idTrajet = idTrajet;
	}

	public String getDepart() {
		return depart;
	}

	public void setDepart(String depart) {
		this.depart = depart;
	}

	public String getArriver() {
		return arriver;
	}

	public void setArriver(String arriver) {
		this.arriver = arriver;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}	

}
