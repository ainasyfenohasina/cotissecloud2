package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class Dbconnection {
    String host = "localhost:5432";
    String dbName = "dbcotisse";
    String user = "cotisse";
    String password = "cotisse";
	
	/*String host = "ec2-174-129-33-156.compute-1.amazonaws.com:5432";
    String dbName = "de3rjmipigkhdg";
    String user = "lypjmvwnzltelm";
    String password = "c98a3425a614aab0b1eddf4d249423e440fb6a114a6a9659b930573b615022b6";
	*/
	
    public Dbconnection(){
        
    }
	
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Connection connect(){
        Connection connection = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://"+host+"/" + dbName, user, password);
            System.out.println("connexion established");
        }catch(Exception e){
            System.out.println("Connection");
        }
        return connection;
    }
}
