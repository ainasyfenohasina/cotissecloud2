package connection;

import classes.Options;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ServiceDAO {
    
    public int getSequence(Connection connection, String seqName)throws Exception{
        String query = "SELECT nextval('MaSeq') as seq";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        ResultSet resultSet = null;
        int result = 0;
        try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    result = resultSet.getInt("seq");
                }
            }catch(Exception e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
        return result;
    }
    
}
