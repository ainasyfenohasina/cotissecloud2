package view;

import classes.OptionsReservationView;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class NbresParMois {
    int nombreplace;
    int mois; 
    int annee; 
    String depart;
    String arriver;
    String idtrajet;
    
    //Method
    public List<NbresParMois> select(Connection connection, String complement) throws Exception {
        List<NbresParMois> result = new ArrayList<NbresParMois>();
        String Query = "select * from NbresParMois " + complement;
        PreparedStatement preparedStat = connection.prepareStatement(Query);
        ResultSet resultSet = null;
        try {
            resultSet = preparedStat.executeQuery();
            while (resultSet.next()) {
                NbresParMois pc = new NbresParMois();
                pc.setNombreplace(resultSet.getInt("nombreplace"));
                pc.setMois(resultSet.getInt("mois"));
                pc.setAnnee(resultSet.getInt("annee"));
                pc.setDepart(resultSet.getString("depart"));
                pc.setArriver(resultSet.getString("arriver"));
                pc.setIdtrajet(resultSet.getString("idtrajet"));
                result.add(pc);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            preparedStat.close();
            resultSet.close();
        }
        return result;
    }

    //construstor
    public NbresParMois(){}
    
    //getter and setter
    public int getNombreplace() {
        return nombreplace;
    }
    public void setNombreplace(int nombreplace) {
        this.nombreplace = nombreplace;
    }
    public int getMois() {
        return mois;
    }
    public void setMois(int mois) {
        this.mois = mois;
    }
    public int getAnnee() {
        return annee;
    }
    public void setAnnee(int annee) {
        this.annee = annee;
    }
    public String getDepart() {
        return depart;
    }
    public void setDepart(String depart) {
        this.depart = depart;
    }
    public String getArriver() {
        return arriver;
    }
    public void setArriver(String arriver) {
        this.arriver = arriver;
    }
    public String getIdtrajet() {
        return idtrajet;
    }
    public void setIdtrajet(String idtrajet) {
        this.idtrajet = idtrajet;
    }
    
}
