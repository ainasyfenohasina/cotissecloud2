package view;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class NbbusParMois {
    int nombrebus;
    int mois; 
    int annee; 
    String depart;
    String arriver;
    String idtrajet;
    
    //method
    public List<NbbusParMois> select(Connection connection, String complement) throws Exception {
        List<NbbusParMois> result = new ArrayList<NbbusParMois>();
        String Query = "select * from NbbusParMois " + complement;
        PreparedStatement preparedStat = connection.prepareStatement(Query);
        ResultSet resultSet = null;
        try {
            resultSet = preparedStat.executeQuery();
            while (resultSet.next()) {
                NbbusParMois pc = new NbbusParMois();
                pc.setNombrebus(resultSet.getInt("nombrebus"));
                pc.setMois(resultSet.getInt("mois"));
                pc.setAnnee(resultSet.getInt("annee"));
                pc.setDepart(resultSet.getString("depart"));
                pc.setArriver(resultSet.getString("arriver"));
                pc.setIdtrajet(resultSet.getString("idtrajet"));
                result.add(pc);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            preparedStat.close();
            resultSet.close();
        }
        return result;
    }
    
    //constructor
    public NbbusParMois(){}
    
    //getter and setter
    public int getNombrebus() {
        return nombrebus;
    }

    public void setNombrebus(int nombrebus) {
        this.nombrebus = nombrebus;
    }

    public int getMois() {
        return mois;
    }

    public void setMois(int mois) {
        this.mois = mois;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArriver() {
        return arriver;
    }

    public void setArriver(String arriver) {
        this.arriver = arriver;
    }

    public String getIdtrajet() {
        return idtrajet;
    }

    public void setIdtrajet(String idtrajet) {
        this.idtrajet = idtrajet;
    }
}
