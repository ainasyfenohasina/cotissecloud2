package view;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;

public class TrajetVoyageVehiculeReservation{
    String idvoyage;
    String idtrajet;
    String idvoiture;
    String marque;
    int placeclient;
    double nbreserver;
    Timestamp datedepart;
    String depart;
    String arriver;
    double distance;
    double duree;
    double puvoyage;
    double estimationrecette;
    
    //constructor
    public TrajetVoyageVehiculeReservation(){}
    
    //method 
    public List<TrajetVoyageVehiculeReservation> select(Connection connection, String complement) throws Exception {
        List<TrajetVoyageVehiculeReservation> result = new ArrayList<TrajetVoyageVehiculeReservation>();
        String Query = "select * from TrajetVoyageVehiculeReservation " + complement;
        PreparedStatement preparedStat = connection.prepareStatement(Query);
        ResultSet resultSet = null;
        resultSet = preparedStat.executeQuery();
        try {
            while (resultSet.next()) {
                TrajetVoyageVehiculeReservation pc = new TrajetVoyageVehiculeReservation();
                pc.setIdvoiture(resultSet.getString("idvoiture"));
                pc.setIdtrajet(resultSet.getString("idtrajet"));
                pc.setIdvoiture(resultSet.getString("idvoiture"));
                pc.setMarque(resultSet.getString("marque"));
                pc.setPlaceclient(resultSet.getInt("placeclient"));
                pc.setNbreserver(resultSet.getDouble("nbreserver"));
                pc.setDatedepart(resultSet.getTimestamp("datedepart"));
                pc.setDepart(resultSet.getString("depart"));
                pc.setArriver(resultSet.getString("arriver"));
                pc.setDistance(resultSet.getDouble("distance"));
                pc.setDuree(resultSet.getDouble("duree"));
                pc.setPuvoyage(resultSet.getDouble("puvoyage"));
                pc.setEstimationrecette(resultSet.getDouble("estimationrecette"));
                result.add(pc);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            preparedStat.close();
            resultSet.close();
        }
        return result;
    }
    
    //getter and setter
    public String getIdvoyage() {
        return idvoyage;
    }

    public void setIdvoyage(String idvoyage) {
        this.idvoyage = idvoyage;
    }

    public String getIdtrajet() {
        return idtrajet;
    }

    public void setIdtrajet(String idtrajet) {
        this.idtrajet = idtrajet;
    }

    public String getIdvoiture() {
        return idvoiture;
    }

    public void setIdvoiture(String idvoiture) {
        this.idvoiture = idvoiture;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public int getPlaceclient() {
        return placeclient;
    }

    public void setPlaceclient(int placeclient) {
        this.placeclient = placeclient;
    }

    public double getNbreserver() {
        return nbreserver;
    }

    public void setNbreserver(double nbreserver) {
        this.nbreserver = nbreserver;
    }

    public Timestamp getDatedepart() {
        return datedepart;
    }

    public void setDatedepart(Timestamp datedepart) {
        this.datedepart = datedepart;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArriver() {
        return arriver;
    }

    public void setArriver(String arriver) {
        this.arriver = arriver;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDuree() {
        return duree;
    }

    public void setDuree(double duree) {
        this.duree = duree;
    }

    public double getPuvoyage() {
        return puvoyage;
    }

    public void setPuvoyage(double puvoyage) {
        this.puvoyage = puvoyage;
    }

    public double getEstimationrecette() {
        return estimationrecette;
    }

    public void setEstimationrecette(double estimationrecette) {
        this.estimationrecette = estimationrecette;
    }
    
    
    
}