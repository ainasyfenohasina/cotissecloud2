package controller;

import classes.Options;
import connection.Dbconnection;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import view.NbbusParMois;
import view.NbresParMois;

@Path("/stat")
public class StatistiqueController {
    
    Dbconnection Dbcon = new Dbconnection();  
    
    @GET
    @Path("/nbresparmois")
    public Response get_NbresParMois() throws Exception{
        NbresParMois stat = new NbresParMois();
        Connection connection  = null;
        List<NbresParMois> result = new ArrayList<NbresParMois>();
        try{
            connection = Dbcon.connect();
            result = stat.select(connection, " ");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<NbresParMois>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    @GET
    @Path("/nbbusparmois")
    public Response get_NbbusParMois() throws Exception{
        NbbusParMois stat = new NbbusParMois();
        Connection connection  = null;
        List<NbbusParMois> result = new ArrayList<NbbusParMois>();
        try{
            connection = Dbcon.connect();
            result = stat.select(connection, " ");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<NbbusParMois>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
}
