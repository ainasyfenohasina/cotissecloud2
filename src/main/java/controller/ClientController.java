package controller;
import classes.Client;
import classes.Token;
import javax.ws.rs.Path;
import connection.Dbconnection;
import java.sql.Connection;
import javax.ws.rs.POST;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

@Path("/client")
public class ClientController{
    Dbconnection Dbcon = new Dbconnection();

    /*login */
    @POST
    @Path("/login")
    public Response login(Client client) throws Exception{
        Connection connection  = null;
        Client client2 = new Client();
        Token token = new Token();
        try{
            connection = Dbcon.connect();
            client2 = client.connectionUser(connection);
            token =  client2.generateToken(connection, client2.getIdClient());
        }catch(Exception e){
            return Response.status(400)
            .entity(new GenericEntity<String>(e.getMessage()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<Token>(token){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }

    /*inscription*/
    @POST
    @Path("/inscription")
    public Response inscription(Client client) throws Exception{
        Connection connection  = null;
        Token token = new Token();
        try{
            connection = Dbcon.connect();
            token = client.inscription(connection);
            
            return Response.status(200)
            .entity(new GenericEntity<Token>(token){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }catch(Exception e){
            return Response.status(400)
            .entity(new GenericEntity<String>(e.getMessage()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
    }

} 