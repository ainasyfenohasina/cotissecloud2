package controller;
import classes.Client;
import classes.Options;
import classes.OptionsReservation;
import classes.OptionsReservationView;
import classes.PlaceClient;
import classes.Reservation;
import classes.TrajetReservation;
import classes.Voyage;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import connection.Dbconnection;
import connection.ServiceDAO;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

@Path("/options")
public class OptionController {
    
    Dbconnection Dbcon = new Dbconnection();
    
    /*option general*/
    @GET
    @Path("/mesOptions")
    public Response get_Options() throws Exception{
        Options option = new Options();
        Connection connection  = null;
        List<Options> result = new ArrayList<Options>();
        try{
            connection = Dbcon.connect();
            result = option.select(connection, " where etat = 1");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<Options>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    //ajouter Option 
    @POST
    @Path("/AjouterOption")
    public Response ajouterOption(Options option) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
            ServiceDAO ser = new ServiceDAO();
            option.setIdOptions("OP"+ser.getSequence(connection, "MaSeq"));
            option.setEtat(1);
            option.insert(connection);
        }catch(Exception e){
            return Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }

    //desactive option
    @POST
    @Path("/disable")
    public Response desactiverOption(Options option) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
            option.Update(connection, "etat=0");
        }catch(Exception e){
            return Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }

    /*option pour chaque client*/
    @GET
    @Path("/MonOption/{idReservation}")
    public Response get_OptionsClient(@PathParam(value="idReservation") String id) throws Exception{
        OptionsReservationView option = new OptionsReservationView();
        Connection connection  = null;
        List<OptionsReservationView> result = new ArrayList<OptionsReservationView>();
        try{
            connection = Dbcon.connect();
            result = option.select(connection, " where idReservation = '"+id+"' and etat = 1");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<OptionsReservationView>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    //ajouter optionToClient
    @POST
    @Path("/AjouterOptionReservation")
    public Response ajouterOptionReservation(OptionsReservation option) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
            ServiceDAO ser = new ServiceDAO();
            option.setIdOptions("OPR"+ser.getSequence(connection, "MaSeq"));
            option.insert(connection);
        }catch(Exception e){
            return Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    //annuler option client
    @GET
    @Path("/AnnulerOption/{idReservation}")
    public Response get_AnnulerOptionsClient(@PathParam(value="idReservation") String id) throws Exception{
        OptionsReservationView option = new OptionsReservationView();
        Connection connection  = null;
        List<OptionsReservationView> result = new ArrayList<OptionsReservationView>();
        try{
            connection = Dbcon.connect();
            result = option.select(connection, " where idReservation = '"+id+"' and etat = 1");
            option = result.get(0);
            option.Update(connection, " etat = 0 ");
        }catch(Exception e){
            return Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<OptionsReservationView>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    
}
