package controller;
import classes.Trajet;
import classes.TrajetReservation;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import connection.Dbconnection;
import connection.ServiceDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

@Path("/trajet")
public class TrajetController {
     
    Dbconnection Dbcon = new Dbconnection();  
    
	//lister trajet
    @GET
    @Path("/liste")
    public Response get_Trajets() throws Exception{
        Trajet trajet = new Trajet();
        Connection connection  = null;
        List<Trajet> result = new ArrayList<Trajet>();
        try{
            connection = Dbcon.connect();
            result = trajet.select(connection, "");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<Trajet>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    //Annuler trajet
    
    @POST
    @Path("/supprimer")
    public Response delete_Trajets(Trajet trajet) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
			trajet.delete(connection);
        }catch(Exception e){
            return Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
         return Response.status(200)
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    /*Inserer nouveau trajet*/
    @POST
    @Path("/new")
    public Response New_Trajet(Trajet nouveauTrajet) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
            ServiceDAO ser = new ServiceDAO();
            nouveauTrajet.setIdTrajet("TR"+ser.getSequence(connection, "MaSeq"));
            nouveauTrajet.insert(connection);
        }catch(Exception e){
            return Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.status(200)
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
}