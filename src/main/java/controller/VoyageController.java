package controller;
import classes.Client;
import classes.PlaceClient;
import classes.Reservation;
import classes.Trajet;
import classes.TrajetReservation;
import classes.Voyage;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import connection.Dbconnection;
import connection.ServiceDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import view.TrajetVoyageVehiculeReservation;

@Path("/voyage")
public class VoyageController {
     
    Dbconnection Dbcon = new Dbconnection();  
    
    @GET
    @Path("/liste")
    public Response get_Voyages() throws Exception{
        Voyage voyage = new Voyage();
        Connection connection  = null;
        List<Voyage> result = new ArrayList<Voyage>();
        try{
            connection = Dbcon.connect();
            result = voyage.select(connection, "");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<Voyage>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    //Annuler voyage
    @POST
    @Path("/Annuler")
    public Response Annuler_Voyages(Voyage voyage) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
            voyage.Update(connection, " etat = 0");
            /*Tous les reservation doivent etre annuler*/
            
            String query = "Update Reservation set etat = 0 where idVoyage like '"+voyage.getIdVoyage()+"'";            
            String query2 = "Update placeClient set etat = 0 where idVoyage like '"+voyage.getIdVoyage()+"'";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.execute();
            ps = connection.prepareStatement(query2);
            ps.execute();
            
            ps.close();
        }catch(Exception e){
           return 
            Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    //-------------
    
    @GET
    @Path("/trajet")
    public Response get_Trajets() throws Exception{
        Trajet trajet = new Trajet();
        Connection connection  = null;
        List<Trajet> result = new ArrayList<Trajet>();
        try{
            connection = Dbcon.connect();
            result = trajet.select(connection, "");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<Trajet>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    @GET
    @Path("/trajet/{depart}/{arriver}")
    public Response get_TrajetsByDestination(@PathParam(value="depart") String depart, @PathParam(value="arriver") String arriver) throws Exception{
        Trajet trajet = new Trajet();
        Connection connection  = null;
        List<Trajet> result = new ArrayList<Trajet>();
        try{
            connection = Dbcon.connect();
            result = trajet.select(connection, " where depart like '"+depart+"' and arriver like '"+arriver+"'");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<Trajet>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    /*Inserer nouveau voyage*/
    @POST
    @Path("/new")
    public Response New_Voyage(Voyage nouveauVoyage) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
            ServiceDAO ser = new ServiceDAO();
            System.out.println("Tafa v lay timetsamp " + nouveauVoyage.getDateDepart());
            nouveauVoyage.setIdVoyage("VY"+ser.getSequence(connection, "MaSeq"));
            nouveauVoyage.setDateDepart(Timestamp.valueOf(nouveauVoyage.getDateDepart2()));
            nouveauVoyage.insert(connection);
        }catch(Exception e){
            return Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.status(200)
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }

    //front fonction --------------------------------------------------------------------------------
    
    /*recherche specifique voyage*/
    @GET
    @Path("/rechercher/{depart}/{arriver}/{date}/{horaire}/{nbPlace}")
    public Response searchVoyageRechercher(@PathParam(value="depart") String depart, @PathParam(value="arriver") String arriver, @PathParam(value="date") String date, @PathParam(value="horaire") String horaire, @PathParam(value="nbPlace") String nbPlace) throws Exception{
        TrajetVoyageVehiculeReservation voyage = new TrajetVoyageVehiculeReservation();
        Connection connection  = null;
        List<TrajetVoyageVehiculeReservation> result = new ArrayList<TrajetVoyageVehiculeReservation>();
        try{
            connection = Dbcon.connect();
            result = voyage.select(connection, " where depart like '"+depart+"' and arriver like '"+arriver+"' and datedepart > '"+date+" "+horaire+"' and (placeclient-nbreserver)>="+nbPlace+" ");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<TrajetVoyageVehiculeReservation>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    /*recherche Voyage by idvoyage*/
    @GET
    @Path("/rechercherId/{idVoyage}")
    public Response searchVoyageRechercher(@PathParam(value="idVoyage") String idVoyage) throws Exception{
        TrajetVoyageVehiculeReservation voyage = new TrajetVoyageVehiculeReservation();
        Connection connection  = null;
        List<TrajetVoyageVehiculeReservation> result = new ArrayList<TrajetVoyageVehiculeReservation>();
        try{
            connection = Dbcon.connect();
            result = voyage.select(connection, " where idvoyage like '"+idVoyage+"' ");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<TrajetVoyageVehiculeReservation>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
}
