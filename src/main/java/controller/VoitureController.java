package controller;

import classes.Client;
import classes.PlaceClient;
import classes.Reservation;
import classes.Trajet;
import classes.TrajetReservation;
import classes.Voiture;
import classes.Voyage;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import connection.Dbconnection;
import connection.ServiceDAO;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

@Path("/voiture")
public class VoitureController {
    
    Dbconnection Dbcon = new Dbconnection();
	
    @GET
    @Path("/liste")
    public Response get_Voiture() throws Exception{
        Voiture voiture = new Voiture();
        Connection connection  = null;
        List<Voiture> result = new ArrayList<Voiture>();
        try{
            connection = Dbcon.connect();
            result = voiture.select(connection, " where disponiblite");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<Voiture>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    //ajout nouvel vehicule
    @POST
    @Path("/nouveau")
    public Response ajout_Voiture(Voiture voiture) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
            ServiceDAO ser = new ServiceDAO();
            voiture.setIdVoiture("V"+ser.getSequence(connection, "MaSeq"));
            voiture.insert(connection);
        }catch(Exception e){
            return Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();  
    }

    //changer etat de la voiture
    @GET
    @Path("/modifierEtat/{idVehicule}/{etat}")
    public Response modif_Voiture(@PathParam(value="idVehicule") String idVehicule, @PathParam(value="etat") String etat) throws Exception{
        Connection connection  = null;
        Voiture voiture = new Voiture();
        try{
            connection = Dbcon.connect();
            ServiceDAO ser = new ServiceDAO();
            List<Voiture> maListe = voiture.select(connection, " where idVoiture like '"+idVehicule+"'");
            voiture = maListe.get(0);
            if(etat.compareToIgnoreCase("indisponible")==0){
                voiture.Update(connection, " disponiblite=0");
            }
            if(etat.compareToIgnoreCase("disponible")==0){
                voiture.Update(connection, " disponiblite=1");
            }
            if(etat.compareToIgnoreCase("enRoute")==0){
                voiture.Update(connection, " disponiblite=11");
            }
        }catch(Exception e){
            return Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.status(200)
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();  
    }
}
