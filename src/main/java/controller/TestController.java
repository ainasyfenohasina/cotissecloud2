package controller;

import classes.Client;
import classes.Reservation;
import classes.TrajetReservation;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import connection.Dbconnection;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@Path("/reds")
public class TestController {

    Dbconnection Dbcon = new Dbconnection();
    
    @GET
    @Path("/msg")
    public String getMsg() {
        return "Hello World !! - Jersey 2";
    }

    @GET
    @Path("/pers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TrajetReservation> get_Reservation() throws Exception{
        TrajetReservation reserv = new TrajetReservation();
        Connection connection  = null;
        List<TrajetReservation> result = new ArrayList<TrajetReservation>();
        try{
            connection = Dbcon.connect();
            result = reserv.select(connection, "");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        
        return result;
    }
	
}
