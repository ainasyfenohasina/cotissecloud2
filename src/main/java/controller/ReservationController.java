package controller;

import classes.Client;
import classes.InsereReservationObjet;
import classes.PlaceClient;
import classes.Reservation;
import classes.TrajetReservation;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import connection.Dbconnection;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import outils.Fonction;

@Path("/resev")
public class ReservationController{

    Dbconnection Dbcon = new Dbconnection();      
    
    @GET
    @Path("/msg")
    public String getMsg() {
        return "Bienvenue dans service reservation";
    }

    @GET
    @Path("/reserV")
    public Response get_Reservation() throws Exception{
        TrajetReservation reserv = new TrajetReservation();
        Connection connection  = null;
        List<TrajetReservation> result = new ArrayList<TrajetReservation>();
        try{
            connection = Dbcon.connect();
            result = reserv.select(connection, "");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<TrajetReservation>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    @GET
    @Path("/reservId/{idReservation}")
    public Response get_ReservationById(@PathParam(value="idReservation") String id) throws Exception{
        Reservation reserv = new Reservation();
        Connection connection  = null;
        List<Reservation> temp = new ArrayList<Reservation>();
        try{
            connection = Dbcon.connect();
            temp = reserv.select(connection, "where idReservation like '"+id+"'");
            reserv = temp.get(0);
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        
        return Response.ok()
            .entity(new GenericEntity<Reservation>(reserv){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    @GET
    @Path("/TrajetreservId/{idReservation}")
    public Response get_TrajetReservationById(@PathParam(value="idReservation") String id) throws Exception{
        TrajetReservation reserv = new TrajetReservation();
        Connection connection  = null;
        List<TrajetReservation> temp = new ArrayList<TrajetReservation>();
        try{
            connection = Dbcon.connect();
            temp = reserv.select(connection, "where idReservation like '"+id+"'");
            reserv = temp.get(0);
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        
        return Response.ok()
            .entity(new GenericEntity<TrajetReservation>(reserv){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    @GET
    @Path("/reservIdValidation/{idReservation}")
    public Response vatlider_ReservationById(@PathParam(value="idReservation") String id) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
            Reservation reserv = new Reservation();
            List<Reservation> temp = new ArrayList<Reservation>();
            temp = reserv.select(connection, " where idReservation like '"+id+"'");
            reserv = temp.get(0);
            reserv.Update(connection, " etat=11 ");
            
            PlaceClient tempClient = new PlaceClient();
            List<PlaceClient> PlaceClients = tempClient.select(connection, " where idVoyage like '"+reserv.getIdVoyage()+"' and idclient like '"+reserv.getIdClient()+"'");
            
            for(int a=0; a<PlaceClients.size(); a++){
                PlaceClients.get(a).Update(connection, " etat=11 ");
            }
            
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    @GET
    @Path("/reservIdAnnulerValidation/{idReservation}")
    public Response AnnulerVatlider_ReservationById(@PathParam(value="idReservation") String id) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
            Reservation reserv = new Reservation();
            List<Reservation> temp = new ArrayList<Reservation>();
            temp = reserv.select(connection, " where idReservation like '"+id+"'");
            reserv = temp.get(0);
            reserv.Update(connection, " etat=1 ");
            
            PlaceClient tempClient = new PlaceClient();
            List<PlaceClient> PlaceClients = tempClient.select(connection, " where idVoyage like '"+reserv.getIdVoyage()+"' and idclient like '"+reserv.getIdClient()+"'");
            
            for(int a=0; a<PlaceClients.size(); a++){
                PlaceClients.get(a).Update(connection, " etat=1 ");
            }
            
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    @GET
    @Path("/reservIdAnnuler/{idReservation}")
    public Response Annuler_ReservationById(@PathParam(value="idReservation") String id) throws Exception{
        Connection connection  = null;
        try{
            connection = Dbcon.connect();
            Reservation reserv = new Reservation();
            List<Reservation> temp = new ArrayList<Reservation>();
            temp = reserv.select(connection, " where idReservation like '"+id+"'");
            reserv = temp.get(0);
            reserv.Update(connection, " etat=0 ");
            
            PlaceClient tempClient = new PlaceClient();
            List<PlaceClient> PlaceClients = tempClient.select(connection, " where idVoyage like '"+reserv.getIdVoyage()+"' and idclient like '"+reserv.getIdClient()+"'");
            
            for(int a=0; a<PlaceClients.size(); a++){
                PlaceClients.get(a).Update(connection, " etat=0 ");
            }
            
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    @GET
    @Path("/PlaceClient/{idClient}/{idVoyage}")
    public Response get_PlaceClientReserver(@PathParam(value="idClient") String idClient, @PathParam(value="idVoyage") String idVoyage) throws Exception{
        PlaceClient reserv = new PlaceClient();
        Connection connection  = null;
        List<PlaceClient> result = new ArrayList<PlaceClient>();
        try{
            connection = Dbcon.connect();
            result = reserv.select(connection, "where idClient like '"+idClient+"' and idVoyage like '"+idVoyage+"'");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        
        return Response.ok()
            .entity(new GenericEntity<List<PlaceClient>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    //front office -------------------------------------------------------------------------------------
    
    @POST
    @Path("/newReservationClient")
    public Response nouvelResevationClient(InsereReservationObjet rescomplet) throws Exception{
        Connection connection = null;
        try{
            connection = Dbcon.connect();
            Fonction f = new Fonction();
            f.fairereservation(connection, rescomplet.getIdVoyage(), rescomplet.getIdClient(), rescomplet.getIdVoiture(), rescomplet.getNumeroConcat(), rescomplet.getPu(), rescomplet.getIdOpCocat());
        
        }catch(Exception e){
            return Response.status(404)
            .entity(new GenericEntity<String>(e.toString()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<String>("ok"){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
    
    @GET
    @Path("/reservationCli/{idClient}")
    public Response avoirMesReservation(@PathParam(value="idClient") String idClient) throws Exception{
        Connection connection = null;
        TrajetReservation TR = new TrajetReservation();
        List<TrajetReservation> result = new ArrayList<TrajetReservation>();
        try{
            connection = Dbcon.connect();
            result = TR.select(connection, " Where idClient like '"+idClient+"'"); 
        }catch(Exception ex){
            return Response.status(400)
            .entity(new GenericEntity<String>(ex.getMessage()){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
            
        }finally{
            connection.close();
        }
        return Response.ok()
            .entity(new GenericEntity<List<TrajetReservation>>(result){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
}
