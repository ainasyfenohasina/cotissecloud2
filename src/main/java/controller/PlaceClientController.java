package controller;

import classes.Client;
import classes.PlaceClient;
import classes.Reservation;
import classes.TrajetReservation;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import connection.Dbconnection;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;


@Path("/placeClient")
public class PlaceClientController {
    Dbconnection Dbcon = new Dbconnection();
     
    //front office ------------------------------------
    
    @GET
    @Path("/placeRes/{idVoyage}")
    public Response getPlaceClientByIdvoyage(@PathParam(value="idVoyage") String idVoyage) throws Exception{
        PlaceClient place = new PlaceClient();
        Connection connection  = null;
        List<PlaceClient> temp = new ArrayList<PlaceClient>();
        try{
            connection = Dbcon.connect();
            temp = place.select(connection, " where idVoyage like '"+idVoyage+"'");
        }catch(Exception e){
            throw e;
        }finally{
            connection.close();
        }
        
        return Response.ok()
            .entity(new GenericEntity<List<PlaceClient>>(temp){})
            .header("Access-Control-Allow-Origin","*")
            .header("Access-Control-Allow-Mehods","GET , POST , DELETE ,PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
    }
}
