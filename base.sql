create database dbcotisse;
create role cotisserole with CREATEDB CREATEROLE;

create user cotisse with password 'cotisse';
grant all privileges on DATABASE dbcotisse TO cotisserole;
grant cotisserole to cotisse;
set role;

/*creae table Client*/
Create Table Client(
	idClient varchar(20) primary key,
	nom varchar(20),
	prenom varchar(20),
	numeroTel varchar(20),
	mdp varchar(500),
	sexe varchar(5),
	etat integer
);

insert into Client values('C1','RABEZAVANA','Jean','0324029863','1234','M', 1);
insert into Client values('C2','RABOZAKA','Jean Noel','0344029863','1234','F', 11);
insert into Client values('C3','RANAVALONA','Christmas','0334029863','1234','M', 21);
insert into Client values('C4','RALAMBO','Nicolas','0324029864','1234','F', 1);

/*Table voiture*/
Create Table Voiture(
	idVoiture varchar(20) primary key,
	marque varchar(30),
	numeroVoiture integer,
	taille integer,
	positionArret varchar(20),
	disponiblite integer
);

insert into Voiture values('V1','sprinter 312D', 1, 27,' ', 1);
insert into Voiture values('V2','sprinter 306D', 1, 27,' ', 1);
insert into Voiture values('V3','crafter', 1, 27,' ', 1);
insert into Voiture values('V4','sprinter', 1, 27,' ', 1);
insert into Voiture values('V5','crafter', 1, 27,' ', 1);
insert into Voiture values('V6','sprinter 412D', 1, 27,' ', 1);
insert into Voiture values('V7','crafter', 1, 27,' ', 1);
insert into Voiture values('V8','crafter', 1, 27,' ', 1);

/*table trajet*/
Create Table Trajet(
	idTrajet varchar(20) primary key,
	depart varchar(20),
	arriver varchar(20),
	distance decimal(10,2)
);

insert into Trajet values('TR1', 'Antananarivo', 'Toamasina', 300);
insert into Trajet values('TR2', 'Toamasina', 'Antananarivo', 300);
insert into Trajet values('TR3', 'Mahajanga', 'Antananarivo', 600);
insert into Trajet values('TR4', 'Antananarivo', 'Mahajanga', 600);
insert into Trajet values('TR5', 'Antsirabe', 'Antananarivo', 120);
insert into Trajet values('TR6', 'Antananarivo', 'Antsirabe', 120);

/*table voyage*/
create Table Voyage(
	idVoyage varchar(20) primary key,
	idTrajet varchar(20),
	idVoiture varchar(20),
	dateDepart Timestamp,
	duree decimal(10,2),
	prix decimal(10,2),
	foreign key (idTrajet) references Trajet(idTrajet),  
	foreign key (idVoiture) references Voiture(idVoiture)  
);

insert into Voyage values('VY1','TR1','V1','2020/01/30 08:00:00', 3.5, 50000);
insert into Voyage values('VY2','TR1','V2','2020/01/30 08:00:00', 3.5, 50000);
insert into Voyage values('VY3','TR2','V3','2020/01/30 08:00:00', 3.5, 50000);
insert into Voyage values('VY4','TR2','V4','2020/01/30 08:00:00', 3.5, 50000);
insert into Voyage values('VY5','TR3','V5','2020/01/31 16:00:00', 8, 30000);
insert into Voyage values('VY6','TR4','V6','2020/01/31 16:00:00', 8, 30000);
insert into Voyage values('VY7','TR5','V7','2020/01/30 08:00:00', 3, 6000);
insert into Voyage values('VY8','TR6','V8','2020/01/30 08:00:00', 3, 6000);

/*table reservation*/
create table Reservation(
	idReservation varchar(20) primary key,
	idVoyage varchar(20),
	idClient varchar(20),
	dateReservation Timestamp,
	nombrePlace decimal(10),
	pu decimal(10,2),
	etat integer,
	foreign key (idVoyage) references Voyage(idVoyage),
	foreign key (idClient) references Client(idClient)
);

insert into Reservation values('R1','VY1','C1','2020/01/07 08:00:00', 5, 50000, 1);
insert into Reservation values('R2','VY1','C2','2020/01/07 08:00:00', 2, 50000, 1);
insert into Reservation values('R3','VY3','C3','2020/01/07 08:00:00', 3, 50000, 1);
insert into Reservation values('R4','VY4','C4','2020/01/07 08:00:00', 1, 50000, 1);
insert into Reservation values('R5','VY6','C4','2020/01/07 08:00:00', 1, 30000, 1);
insert into Reservation values('R6','VY8','C3','2020/01/07 08:00:00', 2, 6000, 1);

/*table Options*/
create table Options(
	idOptions varchar(20) primary key,
	libelle varchar(20),
	prix decimal(10,2),
	etat integer
);

insert into Options values ('OP1','eau', 2500, 1);
insert into Options values ('OP2','couverture', 5000, 1);
insert into Options values ('OP3','snack', 10000, 1);

/*table OptionsReservation*/
create table OptionsReservation(
	idOptionsReservation varchar(20) primary key,
	idOptions varchar(20),
	idReservation varchar(20),
	foreign key (idReservation) references Reservation(idReservation),
	foreign key (idOptions) references Options(idOptions)
);

insert into Optionsreservation values ('OPR1','OP1','R1');
insert into Optionsreservation values ('OPR2','OP3','R1');
insert into Optionsreservation values ('OPR3','OP2','R3');

/*table placeClient*/
Create table  placeClient(
	idPlaceClient varchar(20) primary key,
	idVoyage varchar(20),
	idClient varchar(20),
	idVoiture varchar(20),
	numeroPlace integer,
	etat integer,
	foreign key (idVoyage) references Voyage(idVoyage),
	foreign key (idClient) references Client(idClient),
	foreign key (idVoiture) references Voiture(idVoiture)
);

insert into placeClient values('PCR1','VY1','C1','V1', 4, 1);
insert into placeClient values('PCR2','VY1','C1','V1', 5, 1);
insert into placeClient values('PCR3','VY1','C1','V1', 6, 1);
insert into placeClient values('PCR4','VY1','C1','V1', 7, 1);
insert into placeClient values('PCR5','VY1','C1','V1', 8, 1);
insert into placeClient values('PCR6','VY1','C2','V1', 1, 1);
insert into placeClient values('PCR7','VY1','C2','V1', 2, 1);
insert into placeClient values('PCR8','VY3','C3','V3', 1, 1);
insert into placeClient values('PCR9','VY3','C3','V3', 2, 1);
insert into placeClient values('PCR10','VY3','C3','V3', 3, 1);
insert into placeClient values('PCR11','VY4','C4','V4', 1, 1);
insert into placeClient values('PCR12','VY6','C4','V6', 1, 1);
insert into placeClient values('PCR13','VY8','C3','V8', 2, 1);
insert into placeClient values('PCR14','VY8','C3','V8', 2, 1);

/*table billet*/
create table Billet(
	idBillet varchar(20) primary key,
	idReservation varchar(20),
	nom  varchar(20),
	prenom varchar(20),
	depart varchar(20),
	arriver varchar(20),
	dateDepart Timestamp,
	numeroVoiture decimal(10),
	nombrePlace decimal(10),
	numeroSiege varchar(20),
	prix decimal(10,2),
	foreign key (idReservation) references Reservation(idReservation)
);

/*table payement*/
create table Payement(
	idPayement varchar(20) primary key,
	idReservation varchar(20),
	idClient varchar(20),
	montant decimal(10,2),
	foreign key (idReservation) references Reservation(idReservation),
	foreign key (idClient) references Client(idClient)
);


/*view backoffice*/ 
Create view trajetReservation as select Reservation.idreservation, Trajet.depart, Trajet.arriver, Voyage.dateDepart, Reservation.dateReservation, Client.nom, Client.prenom, Reservation.nombrePlace, Reservation.etat from Reservation join Voyage on Reservation.idVoyage like Voyage.idVoyage join Trajet on Voyage.idTrajet like Trajet.idTrajet join Client on Client.idClient like Reservation.idClient; 


 
 